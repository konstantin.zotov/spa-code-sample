export default {
    totalRecords: state => state.nav.total,
    currentPage: state => state.nav.current,
    pageLimit: state => state.nav.limit,
};
