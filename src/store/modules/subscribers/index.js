import { defaultSubscribersFilter } from '@/constants/ui/filters';
import { defaultNavParams, defaultSubscribersSorting } from '@/constants/ui/listings';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
    namespaced: true,
    state: {
        list: [], // server list of subscribers
        nav: { ...defaultNavParams },
        sorting: { ...defaultSubscribersSorting },
        filter: { ...defaultSubscribersFilter },
    },
    getters,
    mutations,
    actions,
};
