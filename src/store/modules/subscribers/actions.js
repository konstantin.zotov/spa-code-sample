import {
    pagginationQuery, sortQuery, filterQuery, checkIdentical,
} from '@bloomio/spa-admin-common/utils';
import { ERROR_STATUSES } from '@/constants/notifications';
import { GET_SUBSCRIBERS_LIST, GET_SUBSCRIBERS_FILTER_DICTIONARY } from '@/constants/permissions';

export default {
    /**
     * Get list of subscribers with applying filter, sorting and pagination
     * @param {Object} context - store context
     * @returns {Promise} result state of request
     */
    async getList(context) {
        try {
            const res = await this.$http.get(`${context.rootGetters.route(GET_SUBSCRIBERS_LIST)}?${
                pagginationQuery(context.getters.pageLimit, context.getters.currentPage)}${
                filterQuery(context.state.filter)}${
                sortQuery(context.state.sorting)}`);
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data.subscriptions) {
                    context.commit('setList', res.data.data.subscriptions);
                    context.commit('setNavParams', { total: res.data.data.total, start: res.data.data.start, limit: res.data.data.limit });
                } else if (res.data.data.wrong_values) {
                    context.commit('clearList');
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.GET_SUBSCRIBERS_LIST));
            return Promise.reject(err);
        }
    },
    /**
     * Change sorting column for subscriber list
     * @param {Object} context - store context
     * @param {Object} sortObj - sorting params { prop, order }
     * @returns {Promise} success or error state
     */
    async changeSorting(context, sortObj) {
        try {
            if (context.state.sorting.prop !== sortObj.prop) {
                context.commit('setCurrentPage', 1);
            }
            context.commit('setSorting', sortObj);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Apply filter for subscriber list
     * @param {Object} context - store context
     * @param {Object} filterData - filter params
     * @returns {Promise} success or error state
     */
    async filterList(context, filterData) {
        try {
            if (checkIdentical(context.state.filter, filterData)) {
                return Promise.reject(new Error('filter not changed'));
            }
            context.commit('setFilter', filterData);
            context.commit('setCurrentPage', 1);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Get dictionaries for the expert complex filter
     * @param {Object} context - vuex store context
     */
    async getFilterDictionary(context) {
        try {
            const res = await this.$http.get(context.rootGetters.route(GET_SUBSCRIBERS_FILTER_DICTIONARY));
            if (res.data && res.data.success && res.data.data && res.data.data.dictionaries) {
                context.commit('setDictionary', res.data.data.dictionaries, { root: true });
            }
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.GET_SUBSCRIBERS_FILTER_DICTIONARY));
        }
    },
};
