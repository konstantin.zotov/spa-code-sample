import { defaultSubscribersSorting, defaultNavParams } from '@/constants/ui/listings';
import { updateObjectPropsFromURL, updateSortingFromURL, updateNavParamsFromURL } from '@/utils';

export default {
    setList(state, payload) {
        state.list = payload;
    },
    clearList(state) {
        state.list = [];
    },
    setNavParams(state, payload) {
        state.nav = { ...state.nav, ...payload };
    },
    setCurrentPage(state, value) {
        state.nav.current = value;
    },
    setPageLimit(state, value) {
        state.nav.limit = value;
    },
    setSorting(state, value) {
        state.sorting = { ...value };
    },
    setFilter(state, filter) {
        state.filter = { ...filter };
    },
    setQueryParams(state, payload) {
        const {
            page, limit, order, ...filter
        } = payload.params;
        state.filter = updateObjectPropsFromURL(state.filter, filter);
        state.sorting = updateSortingFromURL(order, defaultSubscribersSorting);
        state.nav = updateNavParamsFromURL({ page, limit }, defaultNavParams);
    },
};
