export default {
    allUsersTotal: state => state.all_users_nav.total,
    allUsersCurrentPage: state => state.all_users_nav.current,
    allUsersPageLimit: state => state.all_users_nav.limit,

    investorsTotal: state => state.investors_nav.total,
    investorsCurrentPage: state => state.investors_nav.current,
    investorsPageLimit: state => state.investors_nav.limit,

    startupsTotal: state => state.startups_nav.total,
    startupsCurrentPage: state => state.startups_nav.current,
    startupsPageLimit: state => state.startups_nav.limit,

    userDetailsType: state => (state.current_details.account_type ? state.current_details.account_type : 'user'),
    dictionary: state => (paramId) => {
        const dictObj = state.dictionaries[paramId];
        if (state.dictionaries[paramId] && typeof dictObj === 'string') {
            return state.dictionaries[dictObj];
        }
        return dictObj;
    },
    dictionaryValue: (state, getters) => (paramId, paramKey) => {
        if (Array.isArray(paramKey)) {
            return paramKey.reduce((acc, key) => {
                acc.push(getters.dictionary(paramId)[key]);
                return acc;
            }, []);
        }
        return getters.dictionary(paramId)[paramKey];
    },
    dictionaryArray: (state, getters) => (paramId) => {
        const dictionary = getters.dictionary(paramId);
        if (dictionary) {
            return Object.entries(dictionary).reduce((acc, [k, v]) => {
                acc.push({ value: k, label: v });
                return acc;
            }, []);
        }
        return [];
    },
    activeCollapseSections: state => state.active_sections,
    pendingProfileType: state => state.current_details.pending_profile_type,
};
