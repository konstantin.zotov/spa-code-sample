import {
    pagginationQuery, sortQuery, filterQuery, checkIdentical,
} from '@bloomio/spa-admin-common/utils';
import { ERROR_STATUSES } from '@/constants/notifications';
import {
    GET_USER_LIST,
    GET_INVESTOR_LIST,
    GET_STARTUP_LIST,
    GET_USER_DETAILS,
    GET_USER_FILTER_DICTIONARY,
} from '@/constants/permissions';

export default {
    /**
     * Get list of all users
     * @param {Object} context - store context
     * @returns {Promise} result state of request
     */
    async getUsersList(context) {
        try {
            const res = await this.$http.get(`${context.getters.route(GET_USER_LIST)}?${
                pagginationQuery(context.getters.allUsersPageLimit, context.getters.allUsersCurrentPage)}${
                filterQuery(context.state.users_filter)}${
                sortQuery(context.state.all_users_sorting)}`);
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data.users) {
                    context.commit('setAllUsersList', res.data.data.users);
                    context.commit('setAllUsersNavParams', { total: res.data.data.total, start: res.data.data.start, limit: res.data.data.limit });
                } else if (res.data.data.wrong_values) {
                    context.commit('clearUsersList');
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.USERS_LIST));
            return Promise.reject(err);
        }
    },
    /**
     * Change sorting column for user list
     * @param {Object} context - store context
     * @param {Object} sortObj - sorting params { prop, order }
     * @returns {Promise} success or error state
     */
    async changeUsersSorting(context, sortObj) {
        try {
            if (context.state.all_users_sorting.prop !== sortObj.prop) {
                context.commit('setUsersCurrentPage', 1);
            }
            context.commit('setUsersSorting', sortObj);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Apply filter for user list
     * @param {Object} context - store context
     * @param {Object} filterData - filter params
     * @returns {Promise} success or error state
     */
    async filterUsersList(context, filterData) {
        try {
            if (checkIdentical(context.state.users_filter, filterData)) {
                return Promise.reject(new Error('filter not changed'));
            }
            context.commit('setUsersFilter', filterData);
            context.commit('setUsersCurrentPage', 1);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Get list of investors
     * @param {Object} context - store context
     * @returns {Promise} result state of request
     */
    async getInvestorsList(context) {
        try {
            const res = await this.$http.get(`${context.getters.route(GET_INVESTOR_LIST)}?${
                pagginationQuery(context.getters.investorsPageLimit, context.getters.investorsCurrentPage)}${
                filterQuery(context.state.investors_filter)}${
                sortQuery(context.state.investors_sorting)}`);
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data.users) {
                    context.commit('setInvestorsList', res.data.data.users);
                    context.commit('setInvestorsNavParams', { total: res.data.data.total, start: res.data.data.start, limit: res.data.data.limit });
                } else if (res.data.data.wrong_values) {
                    context.commit('clearInvestorsList');
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.USERS_LIST));
            return Promise.reject(err);
        }
    },
    /**
     * Change sorting column for investor list
     * @param {Object} context - store context
     * @param {Object} sortObj - sorting params { prop, order }
     * @returns {Promise} success or error state
     */
    async changeInvestorsSorting(context, sortObj) {
        try {
            if (context.state.investors_sorting.prop !== sortObj.prop) {
                context.commit('setInvestorsCurrentPage', 1);
            }
            context.commit('setInvestorsSorting', sortObj);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Apply filter for investor list
     * @param {Object} context - store context
     * @param {Object} filterData - filter params
     * @returns {Promise} success or error state
     */
    async filterInvestorsList(context, filterData) {
        try {
            if (checkIdentical(context.state.investors_filter, filterData)) {
                return Promise.reject(new Error('filter not changed'));
            }
            context.commit('setInvestorsFilter', filterData);
            context.commit('setInvestorsCurrentPage', 1);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Get list of startups
     * @param {Object} context - store context
     * @returns {Promise} result state of request
     */
    async getStartupsList(context) {
        try {
            const res = await this.$http.get(`${context.getters.route(GET_STARTUP_LIST)}?${
                pagginationQuery(context.getters.startupsPageLimit, context.getters.startupsCurrentPage)}${
                filterQuery(context.state.startups_filter)}${
                sortQuery(context.state.startups_sorting)}`);
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data.users) {
                    context.commit('setStartupsList', res.data.data.users);
                    context.commit('setStartupsNavParams', { total: res.data.data.total, start: res.data.data.start, limit: res.data.data.limit });
                } else if (res.data.data.wrong_values) {
                    context.commit('clearStartupsList');
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.USERS_LIST));
            return Promise.reject(err);
        }
    },
    /**
     * Change sorting column for startups listing
     * @param {Object} context - store context
     * @param {Object} sortObj - sorting params { prop, order }
     * @returns {Promise} success or error state
     */
    async changeStartupsSorting(context, sortObj) {
        try {
            if (context.state.startups_sorting.prop !== sortObj.prop) {
                context.commit('setStartupsCurrentPage', 1);
            }
            context.commit('setStartupsSorting', sortObj);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Apply filter for startups listing
     * @param {Object} context - store context
     * @param {Object} filterData - filter params
     * @returns {Promise} success or error state
     */
    async filterStartupsList(context, filterData) {
        try {
            if (checkIdentical(context.state.startups_filter, filterData)) {
                return Promise.reject(new Error('filter not changed'));
            }
            context.commit('setStartupsFilter', filterData);
            context.commit('setStartupsCurrentPage', 1);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Get detail information about user
     * @param {Object} context - store context
     * @param {String} id - user id
     * @returns {undefined}
     */
    async getUserById(context, id) {
        try {
            const res = await this.$http.get(context.getters.route(GET_USER_DETAILS, { id }));
            if (res.data && res.data.success && res.data.data && res.data.data.user) {
                context.commit('setCurrentDetails', res.data.data.user);
                context.commit('setDictionaries', res.data.data.form);
                if (res.data.data.form && res.data.data.form.fields) {
                    context.commit('setEditableFormFields', res.data.data.form.fields);
                }
            } else {
                throw new Error('Invalid server answer');
            }
            return Promise.resolve(res.data.data.user);
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.USER_DETAILS));
            return Promise.reject(err);
        }
    },
    /**
     * Get dictionary for user filters
     * @param {Object} context - store context
     * @returns {Promise} state of requset
     */
    async getDictionaries(context) {
        try {
            const result = await Promise.all([
                this.$http.get(context.getters.route(GET_USER_FILTER_DICTIONARY)),
            ]);
            if (result && Array.isArray(result) && result.length) {
                context.commit('setDictionary', { ...result[0].data.data.filter_dictionary });
            }
            return result;
        } catch (err) {
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.FILTER_DICTIONARY));
            return Promise.reject(err);
        }
    },
    /**
     * Download listing data as csv file
     * @param {Object} context - vuex store context
     * @param {Object} payload - API route name and filter parameters
     * @returns {Promise} state of requset
     */
    async downloadListing(context, { routeName, filter }) {
        try {
            const res = await this.$http.get(`${context.getters.route(routeName)}?${filterQuery(filter)}`);
            return Promise.resolve(res);
        } catch (error) {
            context.dispatch('handleApiError', context.getters.serverError(error, ERROR_STATUSES[routeName]));
            return Promise.reject(error);
        }
    },
};
