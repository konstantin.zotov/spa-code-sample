import { updateObjectPropsFromURL, updateSortingFromURL, updateNavParamsFromURL } from '@/utils';
import { defaultUsersFilter, defaultInvestorsFilter, defaultStartupsFilter } from '@/constants/ui/filters';
import { defaultSorting, defaultNavParams } from '@/constants/ui/listings';

export default {
    setAllUsersList(state, payload) {
        state.all_users = payload;
    },
    clearUsersList(state) {
        state.all_users = [];
    },
    setAllUsersNavParams(state, payload) {
        state.all_users_nav = { ...state.all_users_nav, ...payload };
    },
    setUsersCurrentPage(state, value) {
        state.all_users_nav.current = value;
    },
    setUsersPageLimit(state, value) {
        state.all_users_nav.limit = value;
    },
    setUsersSorting(state, value) {
        state.all_users_sorting = { ...value };
    },
    setUsersFilter(state, filter) {
        state.users_filter = { ...filter };
    },
    clearUsersFilter(state) {
        state.users_filter = { ...defaultUsersFilter };
    },

    setInvestorsList(state, payload) {
        state.investors = payload;
    },
    clearInvestorsList(state) {
        state.investors = [];
    },
    setInvestorsNavParams(state, payload) {
        state.investors_nav = { ...state.investors_nav, ...payload };
    },
    setInvestorsCurrentPage(state, value) {
        state.investors_nav.current = value;
    },
    setInvestorsPageLimit(state, value) {
        state.investors_nav.limit = value;
    },
    setInvestorsSorting(state, value) {
        state.investors_sorting = { ...value };
    },
    setInvestorsFilter(state, filter) {
        state.investors_filter = { ...filter };
    },
    clearInvestorsFilter(state) {
        state.investors_filter = { ...defaultInvestorsFilter };
    },

    setStartupsList(state, payload) {
        state.startups = payload;
    },
    clearStartupsList(state) {
        state.startups = [];
    },
    setStartupsNavParams(state, payload) {
        state.startups_nav = { ...state.startups_nav, ...payload };
    },
    setStartupsCurrentPage(state, value) {
        state.startups_nav.current = value;
    },
    setStartupsPageLimit(state, value) {
        state.startups_nav.limit = value;
    },
    setStartupsSorting(state, value) {
        state.startups_sorting = { ...value };
    },
    setStartupsFilter(state, filter) {
        state.startups_filter = { ...filter };
    },
    clearStartupsFilter(state) {
        state.startups_filter = { ...defaultStartupsFilter };
    },

    setCurrentDetails(state, payload) {
        state.current_details = { ...payload };
    },
    updateCurrentDetails(state, updatedParams) {
        const ignoredFields = ['bookmarks', 'pending_profile'];
        Object.keys(updatedParams).forEach((uKey) => {
            if (Object.prototype.hasOwnProperty.call(state.current_details, uKey) && ignoredFields.indexOf(uKey) === -1) {
                state.current_details[uKey] = updatedParams[uKey];
            }
        });
        state.current_details.bookmarks = (updatedParams.bookmarks && updatedParams.bookmarks.length)
            ? state.current_details.bookmarks.filter(b => updatedParams.bookmarks.indexOf(b.company_id) > -1) : [];

        if (updatedParams.pending_profile) {
            state.current_details.pending_profile = (!updatedParams.pending_profile.team && !updatedParams.pending_profile.fields
                && !updatedParams.pending_profile.extra_fields) ? '' : { ...updatedParams.pending_profile };
        }
        state.current_details = { ...state.current_details };
    },
    clearCurrentDetails(state) {
        state.current_details = {};
    },
    setDictionary(state, payload) {
        state.filter_dictionary = { ...state.filter_dictionary, ...payload };
    },
    updateParamsFromQuery(state, payload) {
        const {
            page, limit, order, ...filter
        } = payload.params;
        switch (payload.type) {
        case 'users':
            state.users_filter = updateObjectPropsFromURL(state.users_filter, filter);
            state.all_users_sorting = updateSortingFromURL(order, defaultSorting);
            state.all_users_nav = updateNavParamsFromURL({ page, limit }, defaultNavParams);
            break;
        case 'investors':
            state.investors_filter = updateObjectPropsFromURL(state.investors_filter, filter);
            state.investors_sorting = updateSortingFromURL(order, defaultSorting);
            state.investors_nav = updateNavParamsFromURL({ page, limit }, defaultNavParams);
            break;
        case 'startups':
            state.startups_filter = updateObjectPropsFromURL(state.startups_filter, filter);
            state.startups_sorting = updateSortingFromURL(order, defaultSorting);
            state.startups_nav = updateNavParamsFromURL({ page, limit }, defaultNavParams);
            break;
        default: break;
        }
    },
    setDictionaries(state, payload) {
        if (payload) {
            if (payload.dictionaries) {
                state.dictionaries = payload.dictionaries;
            }
            if (payload.options) { // merge options
                state.dictionaries = { ...state.dictionaries, ...payload.options };
            }
        }
    },
    updateActiveSections(state, payload) {
        state.active_sections = payload;
    },
};
