import {
    filterDictionary, defaultUsersFilter, defaultInvestorsFilter, defaultStartupsFilter,
} from '@/constants/ui/filters';
import { defaultNavParams, defaultSorting } from '@/constants/ui/listings';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';


export default {
    state: {
        all_users: [], // server list of all users
        all_users_nav: { ...defaultNavParams }, // current listing navigation params
        all_users_sorting: { ...defaultSorting }, // current sorting params of user listing
        users_filter: { ...defaultUsersFilter }, // current filter params for user listing
        investors: [], // server list of investors
        investors_nav: { ...defaultNavParams }, // current listing navigation params for investor listing
        investors_sorting: { ...defaultSorting }, // current sorting params of investor listing
        investors_filter: { ...defaultInvestorsFilter }, // current filter params for investor listing
        startups: [], // server list  of startups
        startups_nav: { ...defaultNavParams }, // current listing navigation params for startup listing
        startups_sorting: { ...defaultSorting }, // current sorting params of startup listing
        startups_filter: { ...defaultStartupsFilter }, // current filter params for startup listing
        filter_dictionary: filterDictionary, // dictionaries for filters in listing pages
        dictionaries: {}, // dictionaries for user details page
        current_details: {}, // server user data
        active_sections: [], // active collapse sections on details page
    },
    getters,
    mutations,
    actions,
};
