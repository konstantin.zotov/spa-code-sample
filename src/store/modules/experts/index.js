import { defaultExpertsFilter } from '@/constants/ui/filters';
import { defaultNavParams, defaultExpertsSorting } from '@/constants/ui/listings';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
    namespaced: true,
    state: {
        list: [], // server list of all experts
        nav: { ...defaultNavParams }, // current navigation params of expert listing
        sorting: { ...defaultExpertsSorting }, // current sorting params of expert listing
        filter: { ...defaultExpertsFilter }, // current filter params for expert listing
        current: {}, // server data for detail expert page
        currentForm: {}, // server setting for the form on detail expert page
        currentEditedParams: [], // edited params on expert detail page
    },
    getters,
    mutations,
    actions,
};
