import { ERROR_STATUSES, INFO_STATUSES } from '@/constants/notifications';
import { checkIdentical } from '@bloomio/spa-admin-common/utils';
import { objectDicsToArrayDics } from '@/utils';
import {
    GET_EXPERT_LIST, GET_EXPERT_DETAILS, EDIT_EXPERT_DETAILS, CHECK_EXPERT_URL,
} from '@/constants/permissions';

export default {
    /**
     * Get list of experts
     * @param {Object} context - store context
     * @returns {Promise} result state of request
     */
    async getExpertsList(context) {
        try {
            const res = await this.$http.get(context.rootGetters.route(GET_EXPERT_LIST));
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data.experts) {
                    context.commit('setExpertsList', res.data.data.experts);
                    if (res.data.data.dictionaries) {
                        context.commit('setDictionary', objectDicsToArrayDics(res.data.data.dictionaries), { root: true });
                    }
                } else if (res.data.data.wrong_values) {
                    context.commit('clearExpertsList');
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.rootGetters.serverError(err, ERROR_STATUSES.EXPERTS_LIST), { root: true });
            return Promise.reject(err);
        }
    },
    /**
     * Get an expert details
     * @param {Object} context - vuex store context
     * @param {Object} params - expert id
     * @returns {Promise} result state of request
     */
    async getDetails(context, params) {
        try {
            const res = await this.$http.get(context.rootGetters.route(GET_EXPERT_DETAILS, params));
            if (res.data && res.data.data) {
                if (res.data.success && res.data.data) {
                    res.data.data.expert.id = params.id;
                    context.commit('setCurrentExpert', res.data.data);
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return res;
        } catch (err) {
            context.dispatch('handleApiError', context.rootGetters.serverError(err, ERROR_STATUSES.EXPERT_DETAILS), { root: true });
            return Promise.reject(err);
        }
    },
    /**
     * Submit changes for an expert details
     * @param {Object} context - vuex store context
     * @param {Object} params - expert id
     * @returns {Promise} result state of request
     */
    async submitDetails(context, params) {
        try {
            const res = await this.$http.put(context.rootGetters.route(EDIT_EXPERT_DETAILS, params), context.getters.detailsToSubmit);
            if (res.data && res.data.success && res.data.data && res.data.data.changes) {
                context.commit('updateCurrentExpert', res.data.data.changes);
                context.commit('setValidationMessages', { data: {}, clearOld: true }, { root: true });
            }
            context.dispatch('handleApiInfo', INFO_STATUSES.EDIT_EXPERT_DETAILS, { root: true });
            return Promise.resolve(res.data.data);
        } catch (err) {
            context.dispatch('handleApiError', context.rootGetters.serverError(err, ERROR_STATUSES.EDIT_EXPERT_DETAILS), { root: true });
            if (err.response && err.response.data && err.response.data.invalid_fields) {
                context.commit('setValidationMessages', { data: err.response.data.invalid_fields, clearOld: true }, { root: true });
                return Promise.reject(err.response.data);
            }
            return Promise.reject(err);
        }
    },
    /**
     * Change sorting for expert list
     * @param {Object} context - store context
     * @param {Object} sortObj - sorting params { prop, order }
     * @returns {Promise} success or error state
     */
    async changeExpertsSorting(context, sortObj) {
        try {
            if (context.state.sorting.prop !== sortObj.prop) {
                context.commit('setExpertsCurrentPage', 1);
            }
            context.commit('setExpertsSorting', sortObj);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Apply new filter for expert list
     * @param {Object} context - store context
     * @param {Object} filterData - filter params
     * @returns {Promise} success or error state
     */
    async filterExpertsList(context, filterData) {
        try {
            if (checkIdentical(context.state.filter, filterData)) {
                return Promise.resolve(new Error('filter not changed'));
            }
            context.commit('setExpertsFilter', filterData);
            context.commit('setExpertsCurrentPage', 1);
            return Promise.resolve();
        } catch (err) {
            return Promise.reject(err);
        }
    },
    /**
     * Validate Expert URI parameter
     * @param {Object} context - store context
     * @param {Object} params - expert id and URI value params
     * @returns {Promise} success or error result of the validation
     */
    async checkURI(context, { params }) {
        try {
            const res = await this.$http.get(context.rootGetters.route(CHECK_EXPERT_URL, params));
            if (res.data && res.data.success && res.data.data) {
                if (!res.data.data.unique) {
                    return Promise.resolve({ invalid: true, message: `Value ${params.uri} already are used for another expert` });
                }
            } else {
                throw new Error('Invalid server structure of answer');
            }
            return Promise.resolve({ invalid: false, message: 'This value can be used as an URI' });
        } catch (error) {
            return Promise.resolve({
                invalid: true,
                message: error.response && error.response.data.message ? error.response.data.message : error.message,
            });
        }
    },
};
