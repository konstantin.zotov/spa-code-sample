import { defaultExpertsFilter } from '@/constants/ui/filters';
import { updateObjectPropsFromURL, updateSortingFromURL, updateNavParamsFromURL } from '@/utils';

export default {
    setExpertsList(state, payload) {
        state.list = payload.map((row) => {
            let status = '';
            if (row.public_profile_path) {
                status = row.is_published ? 'Published' : 'Not Published';
            }
            return { ...row, public_profile_status: status };
        });
    },
    clearExpertsList(state) {
        state.list = [];
    },
    setExpertsNavParams(state, payload) {
        state.nav = { ...state.nav, ...payload };
    },
    setExpertsCurrentPage(state, value) {
        state.nav.current = value;
    },
    setExpertsPageLimit(state, value) {
        state.nav.limit = value;
    },
    setExpertsSorting(state, value) {
        state.sorting = { ...value };
    },
    setExpertsFilter(state, filter) {
        state.filter = { ...filter };
    },
    clearExpertsFilter(state) {
        state.filter = { ...defaultExpertsFilter };
    },
    setExpertsQueryParams(state, payload) {
        const {
            page, limit, order, ...filter
        } = payload.params;
        state.filter = updateObjectPropsFromURL(state.filter, filter);
        state.sorting = updateSortingFromURL(order, state.sorting);
        const start = (page - 1) * limit;
        state.nav = updateNavParamsFromURL({ page, limit, start }, state.nav);
    },
    setParamValue(state, data) {
        const foundInd = state.currentEditedParams.findIndex(v => v.id === data.id);
        if (foundInd !== -1) {
            state.currentEditedParams[foundInd].value = data.value;
        } else {
            state.currentEditedParams.push(data);
        }
    },
    clearParamValue(state, id) {
        const foundInd = state.currentEditedParams.findIndex(v => v.id === id);
        if (foundInd !== -1) {
            state.currentEditedParams.splice(foundInd, 1);
        }
    },
    clearEditedParams(state) {
        state.currentEditedParams = [];
    },
    setCurrentExpert(state, { expert, form }) {
        state.current = expert;
        state.currentForm = form;
    },
    updateCurrentExpert(state, data) {
        state.current = { ...state.current, ...data };
    },
};
