import { expertsFilterSettings } from '@/constants/ui/filters';
import { expertsTable } from '@/constants/ui/listings';
import { getFilteredAndSortedArray } from '@/utils';

export default {
    expertsTotal: (state, getters) => getters.filteredList.length,
    expertsCurrentPage: state => state.nav.current,
    expertsPageLimit: state => state.nav.limit,
    isParamEdited: state => paramId => state.currentEditedParams.find(obj => obj.id === paramId),
    isParamsEdited: state => state.currentEditedParams.length,
    filteredList: state => getFilteredAndSortedArray(
        state.list, state.filter, expertsFilterSettings, state.sorting, expertsTable[state.sorting.prop],
    ),
    filteredWithPagginationList: (state, getters) => {
        const { start, limit } = state.nav;
        return getters.filteredList.slice(start, start + limit);
    },
    detailsToSubmit: (state, getters) => (state.currentForm.fields ? Object.entries(state.currentForm.fields).reduce((acc, [k, v]) => {
        if (v) {
            const editedObj = getters.isParamEdited(k);
            acc[k] = editedObj ? editedObj.value : state.current[k];
        }
        return acc;
    }, {}) : {}),
    isEditableFIeld: state => id => state.currentForm.fields && state.currentForm.fields[id],
    currentDictionaries: (state) => {
        let dicts = {};
        if (state.currentForm.dictionaries && typeof state.currentForm.dictionaries === 'object') {
            dicts = { ...dicts, ...state.currentForm.dictionaries };
        }
        if (state.currentForm.options && typeof state.currentForm.options === 'object') {
            const options = Object.entries(state.currentForm.options).reduce((acc, [k, v]) => {
                if (typeof v === 'string') {
                    acc[k] = state.currentForm.dictionaries[v];
                } else {
                    acc[k] = v;
                }
                return acc;
            }, {});
            dicts = { ...dicts, ...options };
        }
        return dicts;
    },
    dictionaryArray: (state, getters) => (paramId) => {
        const dictionary = getters.currentDictionaries[paramId];
        if (dictionary) {
            return Object.entries(dictionary).reduce((acc, [k, v]) => {
                acc.push({ value: k, label: v });
                return acc;
            }, []);
        }
        return [];
    },
};
