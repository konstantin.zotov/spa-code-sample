export default {
    state: {
        iframeAPI: null,
        player: null,
        apiReady: false,
    },
    mutations: {
        // This code creates a script tag which loads the IFrame Player API and saves result in iframeAPI promise
        setIFrameAPI(state) {
            if (!state.iframeAPI) {
                state.iframeAPI = new Promise((resolve, reject) => {
                    const youtubeScript = document.createElement('SCRIPT');
                    const youtubeSource = 'https://www.youtube.com/iframe_api';
                    youtubeScript.setAttribute('src', youtubeSource);
                    youtubeScript.setAttribute('defer', '');
                    const firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(youtubeScript, firstScriptTag);
                    youtubeScript.onload = data => resolve(data);
                    youtubeScript.onerror = err => reject(err);
                });
            }
        },
        setReadyAPI(state, status) {
            state.apiReady = status;
        },
        createYTPlayer(state, playerSettings) {
            /* eslint-disable */
            try {
                state.player = new YT.Player('player', playerSettings);
            } catch (error) {
                console.log('createYTPlayer Error:', error);
            }
        },
        destroyYTPlayer(state) {
            if (state.player) {
                state.player.stopVideo();
                state.player = null;
            }
        },
    },
    actions: {
        /**
         * Create YT player after IFrame Player API was loaded and inited
         * @param {Object} context - store context
         * @param {Object} settings - settings for YT player
         * @returns {void}
         */
        createYoutubePlayer(context, settings) {
            // callback function for IFrame Player API
            function onYouTubePlayerAPIReady() {
                context.commit('setReadyAPI', true);
                context.commit('createYTPlayer', settings);
            }
            context.commit('setIFrameAPI');
            return context.state.iframeAPI.then(() => {
                // create object of YT player if API is ready or add listener
                if (!context.state.apiReady) {
                    window.onYouTubePlayerAPIReady = onYouTubePlayerAPIReady;
                } else {
                    context.commit('createYTPlayer', settings);
                }
            }).catch(err => err);
        },
    },
};
