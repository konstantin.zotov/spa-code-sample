import { UPDATE_USER_DETAILS, UPDATE_INVESTOR_DETAILS, UPDATE_STARTUP_DETAILS } from '@/constants/permissions';
import { INFO_STATUSES, ERROR_STATUSES } from '@/constants/notifications';

export default {
    /**
     * Submit new user data on details page
     * @param {Object} context - store context
     * @param {Object} payload - request params
     * @returns {Promise} state of requset
     */
    async updateUserDetails(context, payload) {
        try {
            let updateUserRoute;
            switch (payload.userType) {
            case 'investor':
                updateUserRoute = UPDATE_INVESTOR_DETAILS;
                break;
            case 'startup':
                updateUserRoute = UPDATE_STARTUP_DETAILS;
                break;
            default:
                updateUserRoute = UPDATE_USER_DETAILS;
                break;
            }
            const res = await this.$http.put(context.getters.route(updateUserRoute, payload.params), context.getters.editedUserDetails);
            if (res.data && res.data.success && res.data.data && res.data.data.updates) {
                context.commit('updateCurrentDetails', res.data.data.updates);
                context.dispatch('handleApiInfo', INFO_STATUSES.UPDATE_USER_DETAILS);
                context.commit('setValidationMessages', { data: {}, clearOld: true }, { root: true });
            } else {
                throw new SyntaxError('Invalid server response structure');
            }
            return Promise.resolve(res.data.data.updates);
        } catch (err) {
            if (err.name === 'SyntaxError') {
                ERROR_STATUSES.UPDATE_USER_DETAILS.title = 'User data were updated with js errors';
                ERROR_STATUSES.UPDATE_USER_DETAILS.message = err.message;
            }
            context.dispatch('handleApiError', context.getters.serverError(err, ERROR_STATUSES.UPDATE_USER_DETAILS));
            if (err.response && err.response.data && err.response.data.invalid_fields) {
                context.commit('setValidationMessages', { data: err.response.data.invalid_fields, clearOld: true }, { root: true });
                return Promise.reject(err.response.data);
            }
            return Promise.reject(err);
        }
    },
};
