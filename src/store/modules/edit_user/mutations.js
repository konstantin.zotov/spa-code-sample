export default {
    setEditableFormFields(state, newFields) {
        state.editServerFormFields = newFields;
    },
    setNewEditValue(state, data) {
        const foundInd = state.edited_basics.findIndex(v => v.id === data.id);
        if (foundInd !== -1) {
            state.edited_basics[foundInd].value = data.value;
        } else {
            state.edited_basics.push(data);
        }
    },
    clearEditValue(state, id) {
        const foundInd = state.edited_basics.findIndex(v => v.id === id);
        if (foundInd !== -1) {
            state.edited_basics.splice(foundInd, 1);
        }
    },
    clearEditedBasics(state) {
        state.edited_basics = [];
    },
    addDeletedBookmark(state, id) {
        state.deleted_bookmarks.push(id);
    },
    removeDeletedBookmark(state, id) {
        state.deleted_bookmarks = state.deleted_bookmarks.filter(bId => bId !== id);
    },
    setSubscriptions(state, payload) {
        state.subscriptions = payload;
    },

    deleteTeamMember(state, memberInd) {
        state.deleted_team_members.push(memberInd);
    },
    restoreTeamMember(state, { id, member }) {
        state.deleted_team_members = state.deleted_team_members.filter(delId => delId !== id);
        Object.keys(state.profileTeam[id]).forEach((key) => { state.profileTeam[id][key] = member[key]; });
    },
    updateTeamMembers(state, pendingData) {
        if (pendingData.team) {
            state.profileTeam = JSON.parse(JSON.stringify(pendingData.team));
        }
    },
    updateCompanyProfileFields(state, pendingData) {
        if (pendingData.fields) {
            state.profileFields = { ...pendingData.fields };
        }
        if (pendingData.extra_fields) {
            state.profileExtraFields = JSON.parse(JSON.stringify(pendingData.extra_fields));
        }
    },
    updatePendingProfileParam(state, data) {
        const { tableId, paramId, newValue } = data;
        if (tableId === 'fields') {
            state.profileFields[paramId] = newValue;
        } else {
            const pathKeys = tableId.split(':');
            if (pathKeys[0] === 'extra_fields') {
                state.profileExtraFields[pathKeys[1]][paramId] = newValue;
            } else if (pathKeys[0] === 'team') {
                state.profileTeam[pathKeys[1]][paramId] = newValue;
            }
        }
    },
    updateEditedFields(state, {
        tableId,
        paramId,
        oldValue,
        newValue,
    }) {
        if (oldValue !== newValue) {
            if (!state.editedSections[tableId]) {
                state.editedSections[tableId] = {};
            }
            state.editedSections[tableId][paramId] = true;
        } else if (state.editedSections[tableId][paramId]) {
            delete state.editedSections[tableId][paramId];
            if (Object.keys(state.editedSections[tableId]).length === 0) {
                delete state.editedSections[tableId];
            }
        }
        state.editedSections = { ...state.editedSections };
    },
    clearEditedFields(state, params) {
        Object.keys(state.editedSections).forEach((key) => {
            params.forEach((paramKey) => {
                if (key === paramKey || key.includes(paramKey)) {
                    delete state.editedSections[key];
                }
            });
        });
        state.editedSections = { ...state.editedSections };
    },
    clearEditedProfileSections(state) {
        state.editedSections = {};
        state.profileTeam = [];
        state.profileFields = {};
        state.profileExtraFields = [];
    },
    clearDeletedBookmarks(state) {
        state.deleted_bookmarks = [];
    },
    resetEditedUserDetails(state) {
        state.edited_basics = [];
        state.deleted_bookmarks = [];
        state.deleted_team_members = [];
        state.subscriptions = {};
        state.editedSections = {};
    },
    enableEditForTeamMember(state, payload) {
        state.editEnableForTeam[payload.id] = payload.status;
        state.editEnableForTeam = { ...state.editEnableForTeam };
    },
};
