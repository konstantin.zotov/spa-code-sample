export default {
    isEditable: state => (paramId, paramField = null) => {
        if (!paramField) { // boolean flag for paramId
            return Boolean(state.editServerFormFields[paramId]);
        }
        // object value for nested field inside paramId object
        if (typeof state.editServerFormFields[paramId][paramField] === 'object') {
            return Object.values(state.editServerFormFields[paramId][paramField]).filter(val => Boolean(val)).length > 0;
        }
        // boolean value for nested field inside paramId object
        return Boolean(state.editServerFormFields[paramId][paramField]);
    },
    isEditableNestedParam: state => (path, paramId) => {
        const editParams = state.editServerFormFields[path.root][path.field];
        if (typeof editParams === 'boolean') {
            return editParams;
        }
        return Boolean(editParams[paramId]);
    },
    isBasicParamEdited: state => paramId => state.edited_basics.find(obj => obj.id === paramId),
    isBookmarkDeleted: state => id => (state.deleted_bookmarks.findIndex(bId => bId === id) >= 0),
    isTeamMemberEdited: state => ind => state.editedSections[`${ind}`],
    isTeamMemberDeleted: state => ind => state.deleted_team_members.findIndex(id => id === ind) > -1,
    isEditEnabledForTeamMember: state => ind => Boolean(state.editEnableForTeam[ind]),
    hasEditedProfileSections: state => Object.keys(state.editedSections).length > 0,
    wereDetailsEdited: (state, getters) => (state.edited_basics.length || state.deleted_bookmarks.length || state.subscriptions.status
        || state.deleted_team_members.length || getters.hasEditedProfileSections),
    editedUserDetails: (state, getters, rootState) => {
        const ignoredFields = ['bookmarks', 'pending_profile'];
        const submitData = Object.keys(state.editServerFormFields).reduce((acc, key) => {
            if (ignoredFields.indexOf(key) === -1) {
                const editedBP = getters.isBasicParamEdited(key);
                acc[key] = editedBP ? editedBP.value : rootState.users.current_details[key];
            }
            return acc;
        }, {});
        // only active bookmark ids
        if (state.editServerFormFields.bookmarks) {
            submitData.bookmarks = !rootState.users.current_details.bookmarks ? '' : rootState.users.current_details.bookmarks
                .filter(val => state.deleted_bookmarks.indexOf(val.company_id) === -1)
                .map(val => val.company_id);
        }

        // active and disactivated subscriptions
        if (state.editServerFormFields.subscriptions) {
            const serverSubscriptions = { ...rootState.users.current_details.subscriptions };
            submitData.subscriptions = Object.keys(serverSubscriptions).reduce((acc, key) => {
                if (state.subscriptions.status) {
                    const status = Boolean(state.subscriptions.checked.length && state.subscriptions.checked.findIndex(ch => ch === key) > -1);
                    acc[key] = status;
                }
                return acc;
            }, serverSubscriptions);
        }
        // all pending profile sections
        if (state.editServerFormFields.pending_profile) {
            if (rootState.users.current_details.pending_profile) {
                // for teammates: all data or null if teammate is deleted
                const team = rootState.users.current_details.pending_profile.team
                    ? rootState.users.current_details.pending_profile.team.reduce((acc, member, index) => {
                        if (state.deleted_team_members.indexOf(index) >= 0) {
                            acc.push(null);
                        } else {
                            acc.push(state.profileTeam[index]);
                        }
                        return acc;
                    }, []) : '';

                // eslint-disable-next-line prefer-destructuring
                let profileFields = state.profileFields;
                // for profile fields: send only allowed params
                if (typeof state.editServerFormFields.pending_profile.fields === 'object') {
                    profileFields = Object.keys(state.profileFields).reduce((acc, id) => {
                        if (getters.isEditableNestedParam({ root: 'pending_profile', field: 'fields' }, id)) {
                            acc[id] = state.profileFields[id];
                        }
                        return acc;
                    }, {});
                }

                submitData.pending_profile = {
                    fields: profileFields,
                    extra_fields: state.profileExtraFields,
                    team,
                };
            } else {
                submitData.pending_profile = '';
            }
        }
        return submitData;
    },
};
