import mutations from './mutations';
import getters from './getters';
import actions from './actions';

export default {
    state: {
        editServerFormFields: {}, // editable form fields from server
        edited_basics: [], // edited params from first section (basic information)
        deleted_bookmarks: [], // deleted user bookmarks
        subscriptions: {}, // checked subscriptions
        deleted_team_members: [], // indexes of deleted team members (no ids in server data)
        editedSections: {}, // flags for edited diff tables from pending company profile
        profileFields: {}, // current company profile fields data with changes
        profileExtraFields: [], // current company profile extra_fields data with changes
        profileTeam: [], // current company team members data with changes
        editEnableForTeam: {}, // saved flags for enabled edit mode for each team member's section
    },
    mutations,
    getters,
    actions,
};
