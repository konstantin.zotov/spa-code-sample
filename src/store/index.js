import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { BASE_URI } from '@/constants/settings';
import auth from '@bloomio/spa-admin-common/modules/auth';
import permissions from '@bloomio/spa-admin-common/modules/permissions';
import notifications from '@bloomio/spa-admin-common/modules/notifications';
import validation from '@bloomio/spa-admin-common/modules/validation';
import interceptorsSetup from '@bloomio/spa-admin-common/interceptors';
import users from './modules/users';
import experts from './modules/experts';
import editUser from './modules/edit_user';
import youtube from './modules/youtube';
import subscribers from './modules/subscribers';


Vue.use(Vuex);

const $http = axios.create({
    baseURL: BASE_URI,
});
Vuex.Store.prototype.$http = $http; // to have access to axios from every module
Vuex.Store.prototype.$vue = Vue;

const appStore = new Vuex.Store({
    modules: {
        auth,
        permissions,
        notifications,
        validation,
        users,
        editUser,
        youtube,
        experts,
        subscribers,
    },
});

interceptorsSetup(appStore, $http, false);

export default appStore;
