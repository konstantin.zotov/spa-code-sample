import moment from 'moment';

const filters = {
    not_empty(value) {
        return ((typeof value !== 'number' && !value) || (Array.isArray(value) && !value.length) ? '***' : value);
    },
    brackets(value) {
        return `[ ${value} ]`;
    },
    number(value) {
        return Number.parseInt(value, 10);
    },
    date(value) { // server date
        const result = moment(value.substring(0, 19)).format('YYYY-MM-DD');
        return result === 'Invalid date' ? '' : result;
    },
    datetime(value) { // server datetime
        const result = moment(value.substring(0, 19)).format('YYYY-MM-DD HH:mm');
        return result === 'Invalid date' ? '' : result;
    },
    status_icon(value) {
        if (value) {
            return 'el-icon-check';
        }
        if (value === false) {
            return 'el-icon-close';
        }
        return 'el-icon-minus';
    },
    string(value) {
        if (Array.isArray(value)) {
            return value.join(', ');
        }
        return [null, undefined].includes(typeof value) ? '' : value.toString();
    },
};

const install = (Vue) => {
    Object.keys(filters).forEach((key) => {
        Vue.filter(key, filters[key]);
    });
};

export default install;
