import DetailsTemplate from './DetailsTemplate';
import DetailsHeaderList from './DetailsHeaderList';
import DetailsHeaderItem from './DetailsHeaderItem';
import DetailsParamList from './DetailsParamList';
import DetailsParamItem from './DetailsParamItem';

export {
    DetailsTemplate, DetailsHeaderList, DetailsHeaderItem, DetailsParamList, DetailsParamItem,
};
