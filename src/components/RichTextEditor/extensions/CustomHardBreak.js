import { HardBreak } from 'tiptap-extensions';

export default class CustomHardBreak extends HardBreak {
    commands({ type }) {
        return () => (state, dispatch) => dispatch(state.tr.replaceSelectionWith(type.create()));
    }
}
