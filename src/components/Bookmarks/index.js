import Bookmarks from './Bookmarks';
import Bookmark from './Bookmark';

export { Bookmark, Bookmarks };
