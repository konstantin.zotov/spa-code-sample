import Vue from 'vue';
import Router from 'vue-router';
import users from './users';
import details from './details';


Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.NODE_ENV === 'production' ? process.env.VUE_APP_URI_POSTFIX : '/',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('@bloomio/spa-admin-common/components/login-page'),
        },
        ...details,
        ...users,
        {
            path: '*',
            name: 'page404',
            component: () => import('@bloomio/spa-admin-common/components/error-page'),
        },
    ],
});
