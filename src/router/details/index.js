export default [
    {
        path: '/user/:userId',
        name: 'details',
        component: () => import('@/pages/details'),
        meta: {
            title: 'User details',
        },
    },
    {
        path: '/expert/:expertId',
        name: 'expert',
        component: () => import('@/pages/details/expert'),
        meta: {
            title: 'Expert details',
            activeMenuLink: 'experts',
        },
    },
];
