import Users from '@/pages/users';
import Experts from '@/pages/users/experts';

export default [
    {
        path: '/',
        name: 'users',
        component: Users,
        meta: {
            menuLabel: 'All users',
            title: 'Users',
        },
    },
    {
        path: '/investors',
        name: 'investors',
        component: Users,
        meta: {
            menuLabel: 'Investors',
            title: 'Investors',
        },
    },
    {
        path: '/startups',
        name: 'startups',
        component: Users,
        meta: {
            menuLabel: 'Startups',
            title: 'Startups',
        },
    },
    {
        path: '/experts',
        name: 'experts',
        component: Experts,
        meta: {
            menuLabel: 'Experts',
            title: 'Experts',
        },
    },
    {
        path: '/subscribers',
        name: 'subscribers',
        component: Users,
        meta: {
            menuLabel: 'Subscribers',
            title: 'Subscribers',
        },
    },
];
