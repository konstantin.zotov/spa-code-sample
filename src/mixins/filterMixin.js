import Vue from 'vue';
import { mapState } from 'vuex';

export default {
    mounted() {
        this.filterData = { ...this.serverFilter };
    },
    data() {
        return {
            pickerOptions: {
                from: {
                    disabledDate: (time) => {
                        if (this.filterData.to) {
                            return time.getTime() >= this.filterData.to;
                        }
                        return false;
                    },
                },
                to: {
                    disabledDate: (time) => {
                        if (this.filterData.from) {
                            return time.getTime() < this.filterData.from;
                        }
                        return false;
                    },
                },
            },
        };
    },
    computed: {
        ...mapState({
            dictionary: state => state.users.filter_dictionary,
        }),
        filterParams() { /** fullfil data from dictionaries */
            Object.keys(this.filterSettings).forEach((key) => {
                if (this.filterSettings[key].options && this.dictionary[key]) {
                    const options = [];
                    this.dictionary[key].forEach((val) => {
                        if (val) {
                            if (typeof val === 'string') {
                                options.push({ value: val, label: val });
                            } else if (typeof val === 'object' && val.value) {
                                options.push(val);
                            }
                        }
                    });
                    Vue.set(this.filterSettings[key], 'items', options);
                }
            });
            return this.filterSettings;
        },
    },
};
