import Vue from 'vue';

export default {
    computed: {
        dynamicFilter: () => (val, filter) => {
            let value = val;
            if (filter) {
                if (Array.isArray(filter)) {
                    filter.forEach((key) => {
                        value = Vue.filter(key)(value);
                    });
                } else {
                    value = Vue.filter(filter)(value);
                }
            }
            return value;
        },
    },
};
