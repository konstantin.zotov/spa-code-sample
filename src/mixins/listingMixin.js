import {
    checkIdentical, formatSorting, routerQueryFilter, routerQuerySort,
} from '@bloomio/spa-admin-common/utils';

export default {
    watch: {
        $route: {
            handler: function handler(val) {
                if (!Object.keys(val.query).length) {
                    this.updateUrlQuery(); // update URL query from store
                } else {
                    this.uploadListWithQuery(val.name, val.query);
                }
                const queryStr = val.fullPath.includes('?') ? val.fullPath.split('?')[1] : null;
                if (queryStr) {
                    document.title = `${val.meta.title}: ${queryStr}`;
                }
            },
            immediate: true,
        },
        serverFilter(newVal, oldVal) {
            if (!checkIdentical(oldVal, newVal)) {
                this.updateUrlQuery();
            }
        },
        serverSorting(newVal, oldVal) {
            if (!checkIdentical(oldVal, newVal)) {
                this.updateUrlQuery();
            }
        },
    },
    computed: {
        sortSetting: vm => formatSorting(vm.serverSorting, 'client'), // sort setting for the table
        currentFilterComponent: vm => `${vm.$route.name}FilterForm`,
    },
    methods: {
        /**
         * Update filter/sorting/navigation vuex parameters from URL query
         * @param {String} type - listing type
         * @param {Object} query - query params
         */
        setNewUrlParams(type, query) {
            this.$store.commit('updateParamsFromQuery', { params: query, type });
        },
        /**
         * Upload listing data with new query parameters
         * @param {String} type - listing type
         * @param {Object} query - query params
         */
        uploadListWithQuery(type, query) {
            this.setNewUrlParams(type, query);
            this.initLoading(this.getList().then(() => {
                this.$emit('data:loaded', true);
            }));
        },
        /**
         * Compare new and current sort params
         * @param {Object} newParams -new sorting params
         * @returns {Boolean} - flag to change sorting
         */
        needChangeSorting(newParams) {
            if (newParams.prop === this.sortSetting.prop && newParams.order === this.sortSetting.order) {
                return false;
            }
            return true;
        },
        /**
         * Handler for sending request to change data sorting
         * @param {Object} event  - new sort params
         */
        sortHandler(event) {
            if (this.needChangeSorting(event)) {
                this.initLoading(this.changeSorting(formatSorting(event)));
            }
        },
        /**
         * Save new current page value into store and update query URL
         * @param {*} newVal - new value of current page
         */
        changeCurrentPageHandler(newVal) {
            this.setCurrentPage(newVal);
            this.updateUrlQuery();
        },
        /**
         * Save new limit of the records into store and update query URL
         * @param {*} newVal - new value of records limit
         */
        changePageLimitHandler(newVal) {
            if (this.pageLimit !== newVal) {
                this.setCurrentPage(1); // reset paginator's page
            }
            this.setPageLimit(newVal);
            this.updateUrlQuery();
        },
        /**
         * Updating a browser URL query string
         */
        updateUrlQuery() {
            this.$router.replace({
                ...this.$route,
                query: {
                    page: Number(this.paginationPage),
                    limit: Number(this.pageLimit),
                    ...routerQueryFilter(this.serverFilter),
                    ...routerQuerySort(this.serverSorting),
                },
            });
        },
    },
};
