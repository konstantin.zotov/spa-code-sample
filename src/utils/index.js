import { clearObject, getBoolean } from '@bloomio/spa-admin-common/utils';
import { sortNumbers, sortStrings } from '@bloomio/spa-admin-common/utils/sortings';

/**
 * Copy values from vue-router query object into obj parameter
 * @param {Object} obj - current
 * @param {Object} newPropsObj - router object with new props
 * @returns {Object} obj with updated prop values
 */
export function updateObjectPropsFromURL(obj, newPropsObj) {
    const res = { ...obj };
    if (!newPropsObj || !Object.keys(newPropsObj).length) {
        clearObject(res);
    } else {
        Object.entries(newPropsObj).forEach(([k, v]) => {
            if (Array.isArray(obj[k]) && typeof v === 'string') {
                res[k] = v.split(',');
            } else {
                res[k] = v;
            }
        });
    }
    return res;
}

/**
 * Parse vue-router sorting string into obj parameter
 * @param {Object} orderStr - sorting string: e.g. last_name,asc
 * @param {Object} defaultObj - sorting object
 * @returns {Object} new sorting object with updated prop values
 */
export function updateSortingFromURL(orderStr, defaultObj) {
    if (orderStr) {
        const sortParams = orderStr.split(',');
        if (sortParams && sortParams.length === 2) {
            return { prop: sortParams[0], order: sortParams[1] };
        }
    }
    return defaultObj;
}

/**
 * Updates pagination values from vue-router query navigation object
 * @param {Object} obj - new navigation options from query
 * @param {Object} defaultObj - current navigation options
 * @returns {Object} obj with updated prop values
 */
export function updateNavParamsFromURL({ page, limit, start }, defaultObj) {
    const newNavParams = { ...defaultObj };
    if (page) {
        newNavParams.current = Number(page);
    }
    if (limit) {
        newNavParams.limit = Number(limit);
    }
    if (start !== undefined) {
        newNavParams.start = Number(start);
    }
    return newNavParams;
}

/**
 * Apply client-side sorting and filters for the listing (used for the expert list)
 * @param {Array} baseArr - server-side listing
 * @param {Object} filter - current filter values
 * @param {Object} filterSettings - filter settings
 * @param {Object} sorting - current sorting value
 * @param {Object} sortingSettings - sorting settings
 * @returns {Array} filtered list with sorting
 */
export function getFilteredAndSortedArray(baseArr, filter, filterSettings, sorting, sortingSettings) {
    const filteredList = baseArr.reduce((acc, curRow) => {
        const status = Object.entries(filter).every(([fk, fv]) => { // apply current filter
            if (!fv || !Object.prototype.hasOwnProperty.call(curRow, fk) || (Array.isArray(fv) && !fv.length)) {
                return true;
            }
            if (Array.isArray(fv) && Array.isArray(curRow[fk])) { // for multiple select filters
                const stringifyArr = curRow[fk].map(el => el.toString().toLowerCase());
                return fv.every(el => stringifyArr.includes(el));
            }
            const strCurVal = curRow[fk] ? curRow[fk].toString().toLowerCase() : '';
            if (filterSettings[fk] && filterSettings[fk].strict) {
                if (filterSettings[fk].strict === 'binary') {
                    return getBoolean(strCurVal) === getBoolean(fv);
                }
                return strCurVal === fv.toString().toLowerCase();
            }
            return strCurVal.startsWith(fv.toString().toLowerCase());
        });
        if (status) {
            acc.push(curRow);
        }
        return acc;
    }, []);
    // apply current sorting
    const { prop, order } = sorting;
    if (sortingSettings && sortingSettings.sortable) {
        filteredList.sort((a, b) => ((sortingSettings.sortType && sortingSettings.sortType === 'number')
            ? sortNumbers(a, b, prop, order) : sortStrings(a, b, prop, order)));
    }
    return filteredList;
}

/**
 * Convert nested object dictionaries into arrays
 * @param {Object} obj - dictionaries object
 * @returns {Object} object with lists of dictionaries
 */
export function objectDicsToArrayDics(obj) {
    return Object.entries(obj).reduce((acc, [k, v]) => {
        acc[k] = Object.entries(v).map(([key, val]) => ({ value: key, label: val }));
        return acc;
    }, {});
}

/**
 * Delay custom function
 * @param {Function} f - function to delay
 * @param {Number} ms - milliseconds to delay
 */
export function debounce(f, ms) {
    let timer = null;
    return function inner(...args) {
        const onComplete = () => {
            f.apply(this, args);
            timer = null;
        };
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(onComplete, ms);
    };
}

/**
 * Download Blob resource
 * @param {*} blobData - data for creating Blob (string, bytearray etc.)
 * @param {Object} blobOptions - blob options like file content-type
 * @param {String} filename - name of the file
 */
export function downloadFile(blobData, blobOptions, filename) {
    const blob = new Blob(blobData, blobOptions);
    const link = document.createElement('a');
    link.style.display = 'none';
    document.body.appendChild(link);
    window.URL = window.URL || window.webkitURL;
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
    setTimeout(() => {
        // For Firefox it is necessary to add link into DOM and delay revoking the ObjectURL
        document.body.removeChild(link);
        window.URL.revokeObjectURL(blob);
    }, 100);
}

export default {};
