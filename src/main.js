import Vue from 'vue';
import Notifications from 'vue-notification';
import '@/plugins/element';
import { TITLE } from '@/constants/settings';
import { viewport, showNotification, initLoading } from '@bloomio/spa-admin-common/utils/';
import initRouterListener from '@bloomio/spa-admin-common/router/';
import App from './App';
import router from './router';
import store from './store';
import Filters from './filters';

// export common stylus styles
require('@bloomio/spa-admin-common/assets/styles/fonts.styl');
require('@bloomio/spa-admin-common/assets/styles/base.styl');
require('@bloomio/spa-admin-common/assets/styles/notifications.styl');


Vue.use(Notifications);
Vue.use(Filters);

Vue.config.productionTip = false;

Vue.prototype.showNotification = showNotification; // for showing notifications
Vue.prototype.initLoading = initLoading(Vue); // for showing "Loading data" screen
Vue.prototype.viewport = viewport; // for monitoring viewport size
Vue.prototype.$filters = Vue.options.filters; // link to global filters


/**
 * Listener on route path changing to setup page title
 */
initRouterListener(router, { defaultTitle: TITLE, store });

router.beforeEach((to, from, next) => {
    // clear edited data if leave details page
    if (from.name && from.name === 'details' && to.name && to.name !== 'login') {
        store.commit('resetEditedUserDetails');
        store.commit('clearEditedProfileSections');
    }
    next();
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
