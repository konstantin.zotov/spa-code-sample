export const TITLE = 'Bloomio Users';
export const BASE_URI = process.env.VUE_APP_BASE_URI; // URL for server RESTful API
export const DASHBOARD_URL = process.env.VUE_APP_DASHBOARD_URL ? process.env.VUE_APP_DASHBOARD_URL : BASE_URI; // link to dashboard page
export const URI_POSTFIX = process.env.VUE_APP_URI_POSTFIX; // spa postfix (path after base domain name)

export const PermissionsModuleIsOff = false;

export default {};
