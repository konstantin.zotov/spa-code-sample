/**
 * Routes for managing UI system
 */
export const GET_INVESTOR_LIST = 'api-investor-collection';
export const GET_STARTUP_LIST = 'api-startup-collection';
export const GET_USER_LIST = 'api-user-collection';
export const GET_EXPERT_LIST = 'api-expert-list';

export const GET_SUBSCRIBERS_LIST = 'api-subscriber-list';
export const GET_SUBSCRIBERS_FILTER_DICTIONARY = 'api-subscription-filter-dictionary';

export const DOWNLOAD_USER_CSV = 'api-user-collection-download';
export const DOWNLOAD_INVESTOR_CSV = 'api-investor-collection-download';
export const DOWNLOAD_STARTUP_CSV = 'api-startup-collection-download';
export const DOWNLOAD_SUBSCRIBER_CSV = 'api-subscriber-list-download';

export const GET_USER_DETAILS = 'api-user';
export const GET_USER_FILTER_DICTIONARY = 'api-user-filter-dictionary';
export const UPDATE_USER_DETAILS = 'api-update-user';
export const UPDATE_INVESTOR_DETAILS = 'api-update-investor';
export const UPDATE_STARTUP_DETAILS = 'api-update-startup';

export const GET_EXPERT_DETAILS = 'api-expert-details';
export const EDIT_EXPERT_DETAILS = 'api-expert-edit';
export const CHECK_EXPERT_URL = 'api-expert-check-uri';

/**
 * Association permissions with routes
 */
export const ROUTE_PERMISSIONS = {
    all: [GET_USER_LIST, GET_USER_FILTER_DICTIONARY],
    investors: [GET_INVESTOR_LIST, GET_USER_FILTER_DICTIONARY],
    startups: [GET_STARTUP_LIST, GET_USER_FILTER_DICTIONARY],
    details: [GET_USER_DETAILS],
    experts: GET_EXPERT_LIST,
    expert: GET_EXPERT_DETAILS,
    subscribers: [GET_SUBSCRIBERS_LIST, GET_SUBSCRIBERS_FILTER_DICTIONARY],
};
