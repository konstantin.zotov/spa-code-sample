export const binaryDictionary = [
    { value: '1', label: 'Yes' },
    { value: '0', label: 'No' },
];

export const filterDictionary = {
    confirmed: [...binaryDictionary],
    onboarding: [...binaryDictionary],
    published: [...binaryDictionary],
    account_type: [],
    investor_type: [],
    kyc_aml_status: [],
    application: [],
    residence: [],
    public_profile_status: [
        { value: '1', label: 'Published' },
        { value: '0', label: 'Not published' },
    ],
    is_dd: [...binaryDictionary],
    tags: [],
};

export const defaultUsersFilter = {
    user_id: '',
    account_id: '',
    account_type: [],
    first_name: '',
    last_name: '',
    email: '',
    confirmed: '',
    onboarding: '',
    residence: [],
    from: '',
    to: '',
    extra_users: [],
};

export const defaultInvestorsFilter = {
    ...defaultUsersFilter,
    investor_type: [],
    kyc_aml_status: [],
    extra_users: [],
};

export const defaultStartupsFilter = {
    user_id: '',
    account_id: '',
    company: '',
    published: '',
    application: [],
    domain: '',
    first_name: '',
    last_name: '',
    email: '',
    role: '',
    from: '',
    to: '',
    extra_users: [],
};

const FIOemail = {
    first_name: {
        placeholder: 'First Name',
    },
    last_name: {
        placeholder: 'Last Name',
    },
    email: {
        placeholder: 'Email',
    },
};

const fromToDatepicker = {
    from: {
        placeholder: 'Registered from',
        type: 'datepicker',
    },
    to: {
        placeholder: 'Registered to',
        type: 'datepicker',
    },
};

const testUsersCheckboxGroup = {
    extra_users: {
        placeholder: 'Extra user groups',
        type: 'select',
        options: {
            multiple: true,
        },
        items: [
            { value: 'test', label: 'Test users' },
            { value: 'bloomio', label: 'Bloomio users' },
        ],
    },
};

export const usersFilterSettings = {
    user_id: {
        placeholder: 'User ID',
    },
    account_id: {
        placeholder: 'Account ID',
    },
    account_type: {
        placeholder: 'Type',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    ...FIOemail,
    confirmed: {
        placeholder: 'Confirmed',
        type: 'select',
        options: {
            multiple: false,
        },
    },
    onboarding: {
        placeholder: 'Onboarding',
        type: 'select',
        options: {
            multiple: false,
        },
    },
    residence: {
        placeholder: 'Residence',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    ...fromToDatepicker,
    ...testUsersCheckboxGroup,
};

export const investorsFilterSettings = {
    user_id: {
        placeholder: 'Investor ID',
    },
    account_id: {
        placeholder: 'Account ID',
    },
    investor_type: {
        placeholder: 'Type',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    ...FIOemail,
    confirmed: {
        placeholder: 'Confirmed',
        type: 'select',
        options: {
            multiple: false,
        },
    },
    onboarding: {
        placeholder: 'Onboarding',
        type: 'select',
        options: {
            multiple: false,
        },
    },
    kyc_aml_status: {
        placeholder: 'KYC/AML',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    residence: {
        placeholder: 'Residence',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    ...fromToDatepicker,
    ...testUsersCheckboxGroup,
};

export const startupsFilterSettings = {
    user_id: {
        placeholder: 'User ID',
    },
    account_id: {
        placeholder: 'Account ID',
    },
    company: {
        placeholder: 'Company',
    },
    published: {
        placeholder: 'Published',
        type: 'select',
        options: {
            multiple: false,
        },
    },
    application: {
        placeholder: 'Application',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    domain: {
        placeholder: 'Domain',
    },
    ...FIOemail,
    role: {
        placeholder: 'Role',
    },
    ...fromToDatepicker,
    ...testUsersCheckboxGroup,
};

export const defaultExpertsFilter = {
    user_id: '',
    account_id: '',
    first_name: '',
    last_name: '',
    tags: [],
    public_profile_status: '',
    is_dd: '',
};

export const expertsFilterSettings = {
    user_id: {
        placeholder: 'User ID',
        strict: true,
    },
    account_id: {
        placeholder: 'Expert ID',
        strict: true,
    },
    first_name: FIOemail.first_name,
    last_name: FIOemail.last_name,
    tags: {
        placeholder: 'Tags',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    public_profile_status: {
        placeholder: 'Public Profile',
        type: 'select',
        strict: 'binary',
        options: {
            multiple: false,
        },
    },
    is_dd: {
        placeholder: 'Participate in DD',
        type: 'select',
        strict: 'binary',
        options: {
            multiple: false,
        },
    },
};


export const defaultSubscribersFilter = {
    id: '',
    user_id: '',
    startup_id: '',
    email: '',
    type: [],
    from: '',
    to: '',
};

export const subscribersFilterSettings = {
    id: {
        placeholder: 'ID',
    },
    user_id: {
        placeholder: 'User ID',
    },
    startup_id: {
        placeholder: 'Startup ID',
    },
    email: {
        placeholder: 'Email',
    },
    type: {
        placeholder: 'Type',
        type: 'select',
        options: {
            multiple: true,
        },
    },
    from: {
        placeholder: 'Created from',
        type: 'datepicker',
    },
    to: {
        placeholder: 'Created to',
        type: 'datepicker',
    },
};

export default {};
