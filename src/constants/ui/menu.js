export default [
    {
        route: 'users',
        title: 'All users',
    },
    {
        route: 'investors',
        title: 'Investors',
    },
    {
        route: 'startups',
        title: 'Startups',
    },
    {
        route: 'experts',
        title: 'Experts',
    },
    {
        route: 'subscribers',
        title: 'Subscribers',
    },
];
