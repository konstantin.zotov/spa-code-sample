/** Settings for collapse sections in user details page */
export const usersFields = {
    active: ['list_data'],
    sections: {
        list_data: {
            title: 'Basic information',
            data: {
                user_id: {
                    title: 'User ID',
                },
                account_id: {
                    title: 'Account ID',
                },
                account_type: {
                    title: 'Account Type',
                },
                first_name: {
                    title: 'First Name',
                },
                last_name: {
                    title: 'Last Name',
                },
                birth_date: {
                    title: 'Date of birth',
                    filter: 'datetime',
                },
                date_user_registered: {
                    title: 'Date of registration',
                    filter: 'datetime',
                },
                email: {
                    title: 'Email',
                },
                email_confirmed: {
                    title: 'Email Confirmed',
                    readFormat: 'status_icon',
                },
                onboarding_completed: {
                    title: 'Onboarding',
                    readFormat: 'status_icon',
                },
                terms_accepted: {
                    title: 'Terms are accepted',
                    readFormat: 'status_icon',
                },
                active: {
                    title: 'Is active',
                    readFormat: 'status_icon',
                },
                residence_code: {
                    title: 'Residence id',
                },
                language_code: {
                    title: 'Language',
                },
            },
        },
        bookmarks: {
            title: 'Bookmarks',
        },
        subscriptions: {
            title: 'Subscriptions',
        },
    },
};

export const investorsFields = {
    active: ['list_data'],
    titleSection: {
        title: 'Account Info',
        data: {
            account_type: {
                title: 'Account type',
            },
            investor_type: {
                title: 'Investor type',
            },
            kyc_aml_status: {
                title: 'KYC/AML Status',
                type: 'url',
                complex: {
                    url: 'kyc_aml_link',
                    content: 'kyc_aml_status',
                },
            },
            blockchain_account: {
                title: 'Blockchain Account',
                type: 'url',
                complex: {
                    url: 'blockchain_details_link',
                    content: 'blockchain_account',
                    error_title: 'not exist',
                    add_url_on_error: true,
                },
            },
            expert_details_link: {
                title: 'Expert',
                type: 'route',
                conditions: {
                    hasServerParam: 'expert_id',
                },
                complex: {
                    routeName: 'expert',
                    routeParams: { expertId: 'expert_id' },
                    content: {
                        expert_id: {},
                    },
                },
            },
        },
    },
    sections: { // collapse sections
        list_data: {
            title: 'Basic information',
            data: {
                user_id: {
                    title: 'User ID',
                },
                account_id: {
                    title: 'Account ID',
                },
                account_type: {
                    title: 'Account Type',
                },
                is_expert: {
                    title: 'Is expert',
                    readFormat: 'status_icon',
                },
                first_name: {
                    title: 'First Name',
                },
                last_name: {
                    title: 'Last Name',
                },
                email: {
                    title: 'Email',
                },
                email_confirmed: {
                    title: 'Email is confirmed',
                    readFormat: 'status_icon',
                },
                email_confirmation_date: {
                    title: 'Email Confirmed date',
                    filter: 'datetime',
                },
                email_confirmation_code: {
                    title: 'Email Confirmed by code',
                },
                onboarding_completed: {
                    title: 'onBoarding is complete',
                    readFormat: 'status_icon',
                },
                birth_date: {
                    title: 'Date of birth',
                    filter: 'date',
                },
                residence_code: {
                    title: 'Residence id',
                },
                active: {
                    title: 'Is active',
                    readFormat: 'status_icon',
                },
                language_code: {
                    title: 'Language',
                },
                terms_accepted: {
                    title: 'Terms are accepted',
                    readFormat: 'status_icon',
                },
                date_user_created: {
                    title: 'Created at',
                    filter: 'datetime',
                },
                date_user_updated: {
                    title: 'Updated at',
                    filter: 'datetime',
                },
                date_user_registered: {
                    title: 'Registered at',
                    filter: 'datetime',
                },
                date_user_last_login: {
                    title: 'Last login at',
                    filter: 'datetime',
                },
                cookie_accepted: {
                    title: 'Is Cookie agree',
                    readFormat: 'status_icon',
                },
                invitation_from: {
                    title: 'Invite from',
                },
                gems_count: {
                    title: 'Status Gems',
                    filter: 'number',
                },
                social_data: {
                    title: 'Social data',
                    readFormat: 'slot_component',
                },
            },
        },
        bookmarks: {
            title: 'Bookmarks',
        },
        subscriptions: {
            title: 'Subscriptions',
        },
    },
};

export const startupsFields = {
    active: ['list_data'],
    titleSection: {
        title: 'Account Info',
        data: {
            published_profile_link: {
                title: null,
                type: 'url',
                complex: {
                    url: 'published_profile_link',
                    error_title: 'No Public profile link',
                    content: {
                        custom_title: 'Public profile link',
                        published_profile_date: {
                            filter: ['datetime', 'brackets'],
                        },
                    },
                },
            },
            application_link: {
                title: 'Current status',
                type: 'url',
                complex: {
                    url: 'application_link',
                    error_title: 'No application data',
                    content: {
                        application_status: {},
                        application_date: {
                            filter: ['datetime', 'brackets'],
                        },
                    },
                },
            },
        },
    },
    sections: {
        list_data: {
            title: 'Basic information',
            data: {
                user_id: {
                    title: 'User ID',
                },
                account_id: {
                    title: 'Account/Startup ID',
                },
                account_type: {
                    title: 'Account Type',
                },
                first_name: {
                    title: 'First Name',
                },
                last_name: {
                    title: 'Last Name',
                },
                birth_date: {
                    title: 'Date of birth',
                    filter: 'date',
                },
                startup_role: {
                    title: 'Role',
                },
                startup_domain: {
                    title: 'Domain',
                },
                date_user_registered: {
                    title: 'Date of registration',
                    filter: 'datetime',
                },
                email: {
                    title: 'Email',
                },
                email_confirmed: {
                    title: 'Email Confirmed',
                    readFormat: 'status_icon',
                },
                onboarding_completed: {
                    title: 'onBoarding is complete',
                    readFormat: 'status_icon',
                },
                terms_accepted: {
                    title: 'Terms are accepted',
                    readFormat: 'status_icon',
                },
                active: {
                    title: 'Is active',
                    readFormat: 'status_icon',
                },
                language_code: {
                    title: 'Language',
                },
                social_data: {
                    title: 'Social data',
                    readFormat: 'slot_component',
                },
            },
        },
        company_profile: {
            title: 'Detail Draft: Company profile',
            subsections: {
                fields: {
                    labels: {
                        profile_country_code: 'Country',
                        profile_tags: 'Tags',
                        profile_company_name: 'Company name',
                        profile_company_site: 'Web site',
                        profile_company_brief: 'Short description',
                        profile_company_description: 'Full description',
                        profile_logo: 'Logo',
                        profile_cover: 'Cover Image',
                        profile_company_video_url: 'Video',
                        profile_company_region: 'Region',
                        profile_company_post_code: 'Post code',
                        profile_company_city: 'City',
                        profile_company_address: 'Address',
                        profile_company_phone: 'Phone',
                        profile_company_facebook: 'Facebook',
                        profile_company_twitter: 'Twitter',
                        profile_company_linkedin: 'LinkedIn',
                    },
                },
                extra_fields: {
                    labels: {
                        title: 'Title',
                        text: 'Text',
                    },
                },
            },
        },
        team: {
            title: 'Detail Draft: Team',
            labels: {
                member_academicBackground: 'Academic Background (Optional)',
                member_city: 'City',
                member_commitment: 'Commitment',
                member_country: 'Country',
                member_currentOwnership: 'Current Ownership',
                member_firstName: 'First Name',
                member_lastName: 'Last Name',
                member_linkedIn: 'LinkedIn Profile (Optional)',
                member_photo: 'Photo',
                member_professionalBackground: 'Professional Background (Optional)',
                member_role: 'Role',
            },
        },
        bookmarks: {
            title: 'Bookmarks',
        },
        subscriptions: {
            title: 'Subscriptions',
        },
    },
};


/** Edit mode settings */
export const TEXT = 'text';
export const URI_TEXT = 'uri_text';
export const TEXTAREA = 'textarea';
export const EMAIL = 'email';
export const SELECT = 'select';
export const MULTISELECT = 'multiselect';
export const SWITCHER = 'switcher';
export const CHECKBOX = 'checkbox';
export const DATEPICKER = 'date';
export const DATETIMEPICKER = 'datetimepicker';
export const DOMAIN = 'domain';
export const URL = 'url';
export const VIDEO_URL_WITH_WATCHER = 'video_url_with_watcher';
export const IMAGE_URL_WITH_WATCHER = 'img_url_with_watcher';
export const IMAGE_VIEWER = 'image_viewer';
export const VIDEO_VIEWER = 'video viewer';
export const HTML_VIEWER = 'html_viewer';
export const RICH_TEXT_EDITOR = 'rich_text_editor';

export const TAGS = 'tags';
export const STATUS_ICON = 'status_icon';

export const ControlTypes = {
    TEXT,
    TEXTAREA,
    EMAIL,
    SELECT,
    MULTISELECT,
    CHECKBOX,
    SWITCHER,
    DATEPICKER,
    DATETIMEPICKER,
    DOMAIN,
    URL,
    VIDEO_URL_WITH_WATCHER,
    IMAGE_URL_WITH_WATCHER,
    IMAGE_VIEWER,
    VIDEO_VIEWER,
    RICH_TEXT_EDITOR,
    HTML_VIEWER,
};

/** common settings for all user categories in the first collapse section (Basic information) */
export const basicEditParams = {
    first_name: { type: TEXT },
    last_name: { type: TEXT },
    email: { type: EMAIL },
    email_confirmed: { type: CHECKBOX },
    is_expert: { type: CHECKBOX },
    birth_date: { type: DATEPICKER, placeholder: 'Pick a birth date' },
    residence_code: { type: SELECT },
    language_code: { type: SELECT },
    active: { type: CHECKBOX },
    startup_role: { type: TEXT },
    startup_domain: { type: DOMAIN },
};

export const teamEditParams = {
    member_academicBackground: { type: TEXT },
    member_city: { type: TEXT },
    member_commitment: { type: SELECT },
    member_country: { type: SELECT },
    member_currentOwnership: { type: TEXT },
    member_firstName: { type: TEXT },
    member_lastName: { type: TEXT },
    member_linkedIn: { type: TEXT, dataType: URL },
    member_photo: { dataType: IMAGE_VIEWER },
    member_professionalBackground: { type: TEXT },
    member_role: { type: TEXT },
};

export const companyProfileEditParams = {
    profile_company_address: { type: TEXTAREA },
    profile_company_city: { type: TEXT },
    profile_company_name: { type: TEXT },
    profile_company_site: { type: DOMAIN },
    profile_country_code: { type: SELECT },
    profile_company_description: { type: TEXTAREA, rows: 4 },
    profile_logo: { dataType: IMAGE_VIEWER },
    profile_cover: { dataType: IMAGE_VIEWER },
    profile_company_facebook: { type: URL, dataType: URL },
    profile_company_linkedin: { type: URL, dataType: URL },
    profile_company_phone: { type: TEXT },
    profile_company_post_code: { type: TEXT },
    profile_company_region: { type: TEXT },
    profile_company_brief: { type: TEXTAREA, rows: 2 },
    profile_tags: { type: MULTISELECT },
    profile_company_twitter: { type: URL, dataType: URL },
    profile_company_video_url: { type: VIDEO_URL_WITH_WATCHER, dataType: VIDEO_VIEWER, popupTitle: 'Youtube Video of Company Profile' },
};

export const extraFieldsEditParams = {
    title: { type: TEXT },
    text: { type: TEXTAREA, rows: 4 },
};

export const PanelActions = [
    {
        title: 'Discard changes',
        event: 'discard',
        type: 'warning',
        disabled: false,
    },
    {
        title: 'Save',
        event: 'save',
        type: 'success',
        disabled: false,
    },
];

export default {};
