import {
    ControlTypes, IMAGE_VIEWER, TAGS, STATUS_ICON, HTML_VIEWER,
} from '.';

export const detailsFields = {
    user_id: {
        title: 'Investor',
        route: {
            name: 'details',
            params: {
                userId: 'user_id',
            },
        },
    },
    email: {
        title: 'Email',
        editable: false,
        type: ControlTypes.TEXT,
    },
    email_confirmed: {
        title: 'Is email confirmed',
        readFormat: STATUS_ICON,
        editable: false,
        type: ControlTypes.CHECKBOX,
    },
    is_active: {
        title: 'Is active',
        readFormat: STATUS_ICON,
        editable: true,
        type: ControlTypes.CHECKBOX,
    },
    is_dd: {
        title: 'Is participate in dd',
        readFormat: STATUS_ICON,
        editable: true,
        type: ControlTypes.CHECKBOX,
    },
    is_published: {
        title: 'Is published',
        readFormat: STATUS_ICON,
        editable: true,
        type: ControlTypes.CHECKBOX,
    },
    tags: {
        title: 'Tags',
        placeholder: 'Select Tags',
        editable: true,
        type: ControlTypes.MULTISELECT,
        filter: 'string',
        readFormat: TAGS,
    },
    uri: { // autogenerate 'firstname-lastname'
        title: 'URI',
        placeholder: 'Add URI',
        editable: true,
        type: ControlTypes.TEXT,
        defaultTemplate: '%first_name%-%last_name%',
        inputAction: {
            name: 'experts/checkURI',
            routeParams: {
                id: 'id',
                uri: 'uri',
            },
            blockSubmit: true,
        },
    },
    designation: {
        title: 'Designation',
        placeholder: 'Add Designation',
        editable: true,
        type: ControlTypes.TEXT,
    },
    bio: {
        title: 'Bio',
        placeholder: 'Add BIO',
        editable: true,
        type: ControlTypes.RICH_TEXT_EDITOR,
        readFormat: HTML_VIEWER,
        autosize: true,
    },
    linkedin: { // linkedIn url
        title: 'LinkedIn URL',
        editable: true,
        type: ControlTypes.TEXT,
        placeholder: 'Add URL for LinkedIn profile',
        autosize: true,
    },
    photo: { // url of the photo for now
        title: 'Photo',
        editable: true,
        placeholder: 'Add URL for photo',
        type: ControlTypes.TEXT,
        readFormat: IMAGE_VIEWER,
    },
};

export const listStub = [
    {
        user_id: 1111,
        expert_id: 1,
        first_name: 'testfirst',
        last_name: 'testlast',
        tags: 'Software, Fintech',
        public_profile: 'published',
        public_profile_link: '/',
        participate_in_dd: true,
        is_active: true,
        offers: 3,
        in_review: 1,
        completed: 0,
    },
    {
        user_id: 1112,
        expert_id: 2,
        first_name: 'John',
        last_name: 'Smith',
        tags: 'Software, Fintech',
        public_profile: '',
        public_profile_link: '',
        participate_in_dd: false,
        is_active: false,
        offers: 2,
        in_review: 1,
        completed: 1,
    },
    {
        user_id: 111,
        expert_id: 11,
        first_name: 'Ivan',
        last_name: 'Ivanov',
        tags: '',
        public_profile: 'published',
        public_profile_link: '/',
        participate_in_dd: true,
        is_active: true,
        offers: 2,
        in_review: 2,
        completed: 1,
    },
];

export const detailsStub = {
    expert_id: 88,
    user_id: 117,
    first_name: 'Vasja',
    last_name: 'Pupkin',
    email: 'test.email@gmail.com',
    confirmed: true,
    is_active: true,
    participate_in_dd: true,
    published: true,
    tags: ['various', 'blockchain'],
    uri: '',
    designation: '',
    bio: '',
    li_url: '',
    photo: 'http://images6.fanpop.com/image/photos/34700000/man-of-steel-man-of-steel-34785861-584-432.jpg',
};
