export const defaultNavParams = {
    total: 0, // count of records
    current: 1, // current page of listing
    start: 0, // start index of record in server answer
    limit: 50, // records on page
};

export const defaultSorting = {
    prop: 'registered', // name of column
    order: 'desc', // ascending or descending
};

export const defaultExpertsSorting = {
    prop: 'account_id', // name of column
    order: 'desc', // ascending or descending
};

export const defaultSubscribersSorting = {
    prop: 'id',
    order: 'desc',
};

export const usersTable = {
    user_id: {
        title: 'User ID',
        sortable: 'custom',
        link: {
            base: '/user/',
        },
    },
    account_id: {
        title: 'Account ID',
        sortable: 'custom',
    },
    account_type: {
        title: 'Type',
        sortable: 'custom',
    },
    first_name: {
        title: 'First Name',
        sortable: 'custom',
    },
    last_name: {
        title: 'Last Name',
        sortable: 'custom',
    },
    registered: {
        title: 'Registration',
        sortable: 'custom',
        filter: 'datetime',
    },
    email: {
        title: 'Email',
        sortable: 'custom',
    },
    confirmed: {
        title: 'Confirmed',
        sortable: 'custom',
    },
    onboarding: {
        title: 'Onboarding',
        sortable: 'custom',
    },
    residence: {
        title: 'Residence',
        sortable: 'custom',
    },
};

export const investorsTable = {
    user_id: {
        title: 'Investor ID',
        sortable: 'custom',
        link: {
            base: '/user/',
        },
        width: '112',
    },
    account_id: {
        title: 'Account ID',
        sortable: 'custom',
        width: '113',
    },
    investor_type: {
        title: 'Type',
        sortable: 'custom',
        width: '110',
    },
    first_name: {
        title: 'First Name',
        sortable: 'custom',
    },
    last_name: {
        title: 'Last Name',
        sortable: 'custom',
    },
    registered: {
        title: 'Registration',
        sortable: 'custom',
        filter: 'datetime',
    },
    email: {
        title: 'Email',
        sortable: 'custom',
    },
    confirmed: {
        title: 'Confirmed',
        sortable: 'custom',
        width: '110',
    },
    onboarding: {
        title: 'Onboarding',
        sortable: 'custom',
        width: '117',
    },
    kyc_aml_status: {
        title: 'KYC/AML',
        sortable: 'custom',
        width: '108',
    },
    residence: {
        title: 'Residence',
        sortable: 'custom',
        width: '110',
    },
    blockchain_account: {
        title: 'Blockchain Account',
        sortable: false,
        width: '210',
        default_value: 'not exist',
        link: {
            url_param: 'blockchain_details_link',
        },
    },
};

export const startupsTable = {
    user_id: {
        title: 'User ID',
        sortable: 'custom',
        width: '90',
        link: {
            base: '/user/',
        },
    },
    account_id: {
        title: 'Account/Startup ID',
        sortable: 'custom',
        width: '164',
    },
    company_id: {
        title: 'Company ID',
        sortable: false,
    },
    company: {
        title: 'Company',
        sortable: 'custom',
    },
    published: {
        title: 'Published',
        sortable: 'custom',
        complex: {
            url: 'link',
            content: 'date',
            filter: 'datetime',
        },
    },
    application: {
        title: 'Application',
        sortable: 'custom',
        complex: {
            url: 'link',
            content: {
                status: {},
                date: {
                    filter: 'datetime',
                },
            },
        },
    },
    domain: {
        title: 'Domain',
        sortable: 'custom',
    },
    first_name: {
        title: 'First Name',
        sortable: 'custom',
    },
    last_name: {
        title: 'Last Name',
        sortable: 'custom',
    },
    registered: {
        title: 'Registration',
        sortable: 'custom',
        filter: 'datetime',
    },
    email: {
        title: 'Email',
        sortable: 'custom',
    },
    role: {
        title: 'Role',
        sortable: 'custom',
    },
};

export const expertsTable = {
    user_id: {
        title: 'User ID',
        sortable: 'custom',
        sortType: 'number',
        width: '90',
        link: {
            base: '/user/',
        },
    },
    account_id: {
        title: 'Expert ID',
        sortable: 'custom',
        sortType: 'number',
        width: '90',
        link: {
            base: '/expert/',
        },
    },
    first_name: {
        title: 'First Name',
        sortable: 'custom',
    },
    last_name: {
        title: 'Last Name',
        sortable: 'custom',
    },
    tags: {
        title: 'Tags',
        sortable: false,
        filter: 'string',
        dictionary: 'tags',
    },
    public_profile_status: {
        title: 'Public Profile',
        sortable: 'custom',
        link: {
            url_param: 'public_profile_path',
        },
    },
    is_dd: {
        title: 'Participate in DD',
        sortable: 'custom',
        width: '110',
        is_status_icon: true,
    },
    is_active: {
        title: 'Is active',
        sortable: 'custom',
        width: '110',
        is_status_icon: true,
    },
    offer_sent: {
        title: 'Offers',
        sortable: 'custom',
        sortType: 'number',
        width: '90',
    },
    offer_accepted: {
        title: 'In review',
        sortable: 'custom',
        sortType: 'number',
        width: '90',
    },
    review_completed: {
        title: 'Completed',
        sortable: 'custom',
        sortType: 'number',
        width: '90',
    },
};

export const subscribersTable = {
    id: {
        title: 'ID',
        sortable: 'custom',
        width: '90',
    },
    user_id: {
        title: 'User ID',
        sortable: 'custom',
        width: '90',
        link: {
            base: '/user/',
        },
    },
    startup_id: {
        title: 'Startup ID',
        sortable: 'custom',
        width: '90',
    },
    email: {
        title: 'Email',
        sortable: 'custom',
    },
    type: {
        title: 'Type',
        sortable: 'custom',
        dictionary: 'type',
    },
    created: {
        title: 'Created At',
        sortable: 'custom',
        filter: 'datetime',
    },
};

export default {};
