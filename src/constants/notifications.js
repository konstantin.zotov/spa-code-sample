const INFO_STATUSES = {
    UPDATE_USER_DETAILS: {
        message: 'User details were updated successfuly',
    },
    EDIT_EXPERT_DETAILS: {
        message: 'Expert details were updated successfuly',
    },
};

const ERROR_STATUSES = {
    REFRESH_ERROR: {
        title: 'Authorization error',
        message: 'Refresh token is expired. Need relogin',
    },
    USERS_LIST: {
        title: "Can't get list of users",
    },
    USER_DETAILS: {
        title: "Can't get user details",
    },
    FILTER_DICTIONARY: {
        title: "Can't get dictionary for filter",
    },
    UPDATE_USER_DETAILS: {
        title: 'User details weren\'t updated',
        duration: 20000,
    },
    EXPERT_DETAILS: {
        title: "Can't get expert details",
    },
    EDIT_EXPERT_DETAILS: {
        title: 'Expert details weren\'t updated',
        duration: 20000,
    },
    GET_SUBSCRIBERS_LIST: {
        title: "Can't get list of subscribers",
    },
    GET_SUBSCRIBERS_FILTER_DICTIONARY: {
        title: "Can't get dictionary for the subscriber list filter",
    },
};

export { INFO_STATUSES, ERROR_STATUSES };
