# Bloomio Users

## Project setup
```
npm install (installing of all npm packages that are required for the build of application)
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build  (A folder 'dist' will be created in the root of repo. it contains build-files of spa, that should be placed on server in the directory [COMPANIES-DASHBOARD-DIR]/public/[VUE_APP_URI_POSTFIX]/)
```

### Use .env files for setting app to production
```
.env - this file is used in development mode
.env.production - this file is used when creating local production build
.env.stage is used for bloomio stage envirement
.env.prod is used for bloomio prod envirement

VUE_APP_URI_POSTFIX - postfix for spa place on server (e.g. /spa/users/ means that build of spa is located on server in the directory [COMPANIES-DASHBOARD-DIR]/public/spa/users)
VUE_APP_BASE_API - base url of server API for sending requests
VUE_APP_ROOT_URI - path for redirecting to main admin panel
```

### nginx configuration
```
All browser transitions to [ADMIN_DASHBOARD_URL]/spa/users/* urls should result in opening of [COMPANIES-DASHBOARD-DIR]/public/spa/users/index.html
e.g.: http://bloomio.admin.com/spa/users/user/123 => server should give to the browser the page [COMPANIES-DASHBOARD-DIR]/public/spa/users/index.html
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
