const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const OUTPUT_BUILD_DIR = path.resolve(__dirname, process.env.VUE_APP_BUILD_DIR
    ? process.env.VUE_APP_BUILD_DIR : '../symfony/public/');

// the paths (relative to ROOT_DIR) that should be cleaned before build
const pathsToClean = [
    'js',
    'css',
    'fonts',
    'img',
    'favicon.ico',
    'index.html',
];
const cleanOptions = {
    root: OUTPUT_BUILD_DIR,
    verbose: true,
    dry: false,
};

const plugins = process.env.NODE_ENV === 'production' ? [
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
] : [];


function addStyleResource(rule) {
    rule.use('style-resource').loader('style-resources-loader').options({
        patterns: [
            path.resolve(__dirname, './node_modules/@bloomio/spa-admin-common/assets/styles/colors.styl'),
            path.resolve(__dirname, './node_modules/@bloomio/spa-admin-common/assets/styles/variables.styl'),
        ],
    });
}

module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? process.env.VUE_APP_URI_POSTFIX : '/',
    outputDir: OUTPUT_BUILD_DIR,
    chainWebpack: (config) => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
        types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)));
    },
    configureWebpack: {
        performance: {
            hints: false,
        },
        plugins,
    },
    devServer: {
        port: 9091,
        historyApiFallback: true,
        noInfo: true,
        proxy: {
            '/api': {
                target: process.env.VUE_APP_PROXY_API,
                changeOrigin: true,
                secure: false,
                ws: false,
                cookieDomainRewrite: '',
                onProxyReq: (request) => {
                    request.setHeader('origin', process.env.VUE_APP_PROXY_API);
                },
            },
        },
    },
};
